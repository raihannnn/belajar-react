import logo from './logo.svg';
import './App.css';
import React from 'react';

//function App
function App() {
  let nama = 'M Raihan Ramadhan';
  let kelas = "XI RPL";

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <h1>Nama: {nama}</h1>
        <h1>kelas: {kelas}</h1>
        <h1>Ini Function App</h1>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

//const AppVar
const AppVar = ()=>{
  const name = 'M Raihan Ramadhan'
  const kelas = "XI RPL"

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
  <h2>{name}</h2>
  <h2>{kelas}</h2>
<h1>Ini AppVar</h1>
      </header>
    </div>
  );
}

//class AppClass  
class AppClass extends React.Component{
  constructor(props){
    super(props);
    this.name = 'M Raihan Ramadhan'
    this.kelas = "XI RPL"
  }
  render(){
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>

          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
      <h3>{this.name}</h3>
     <h3>{this.kelas}</h3>
     <h1>Ini AppClass</h1>
       
        </header>
      </div>
    );
  }
}

export {
  App,
  AppClass,
  AppVar
}
